# beginner_flutter3

Beginner code for Flutter 3.


## Features

* Light/Dark Mode
* Switchable 10 Color Themes
* 2 Languages, English and Thai
* MessageBox
* WaitBox


## Run

Just type:

	flutter run

Or, specify platform, like:

	flutter run -d [platform]


## Credits

Thank you, [Google Fonts](https://fonts.google.com/icons) API.

Thank you, [Melvin ilham Oktaviansyah](https://freeicons.io/profile/8939) on [freeicons.io](https://freeicons.io) for icons.

Thank you, [icon king1](https://freeicons.io/profile/3) on [freeicons.io](https://freeicons.io) for icons.


## Last

Sorry, but I'm not good at English. T_T

