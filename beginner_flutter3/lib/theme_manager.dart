import 'package:logging/logging.dart';
import 'package:flutter/material.dart';
import './shared_pref.dart';
import './app.dart';

/// ThemeManager service singleton class.
class ThemeManager {
  static ThemeManager? _instance;

  static ThemeManager instance() {
    _instance ??= ThemeManager();
    return _instance!;
  }

  // ----------------------------------------------------------------------

  static final log = Logger('ThemeManager');

  static int defaultColor = 0;

  /// Constructor.
  ThemeManager();

  /// Load.
  void load(ThemeMode mode, int theme) {
    _themeMode = mode;
    if (theme >= 0 && theme < list.length) _index = theme;
  }

  // ----------------------------------------------------------------------

  /// Color list.
  static List<Color> list = [
    Colors.indigo,
    Colors.lightBlue,
    Colors.teal,
    Colors.green,
    Colors.amber,
    Colors.deepOrange,
    Colors.red,
    Colors.pink,
    Colors.purple,
    Colors.deepPurple,
  ];

  /// Current theme.
  Color get current => list[_index];
  int _index = defaultColor;

  /// Changes current theme.
  void change(BuildContext context, int index) {
    if (index < 0 || index >= list.length) return;
    if (_index != index) {
      _index = index;
      log.info("change theme to $_index");
      App.refresh(context);
      SharedPref.instance().saveTheme(_index);
    }
  }

  /// Mode: default, light and dark.
  ThemeMode _themeMode = ThemeMode.system;
  ThemeMode get themeMode => _themeMode;
  void changeMode(BuildContext context, ThemeMode value) {
    _themeMode = value;
    log.info("change mode to $_themeMode");
    App.refresh(context);
    SharedPref.instance().saveMode(_themeMode);
  }
}
