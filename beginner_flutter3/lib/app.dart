import 'package:logging/logging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:page_transition/page_transition.dart';
import './i18n/translations.g.dart';
import './consts.dart';
import './localization.dart';
import './theme_manager.dart';
import './pages/home.dart';
import './pages/test.dart';
import './pages/settings.dart';

/// App class.
class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();

  /// Refresh app.
  static void refresh(BuildContext context) =>
      context.findAncestorStateOfType<_AppState>()?.refresh();
}

// ----------------------------------------------------------------------

class _AppState extends State<App> {
  static final log = Logger('App');

  @override
  void initState() {
    super.initState();
    log.fine("$this initState()");
  }

  @override
  void dispose() {
    log.fine("$this dispose()");
    super.dispose();
  }

  // ----------------------------------------------------------------------

  /// Build widget tree.
  @override
  Widget build(BuildContext context) {
    final t = Translations.of(context).strings;

    return MaterialApp(
      // localization
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: Localization.instance().list,
      locale: Localization.instance().current,

      // themes
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          brightness: Brightness.light,
          seedColor: ThemeManager.instance().current,
        ),
        useMaterial3: true,
      ),
      darkTheme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          brightness: Brightness.dark,
          seedColor: ThemeManager.instance().current,
        ),
        useMaterial3: true,
      ),
      themeMode: ThemeManager.instance().themeMode,

      // route
      onGenerateRoute: (settings) {
        var path = settings.name ?? '';
        switch (path) {
          case Consts.testPage:
            return _buildRoute(settings, const TestPage());

          case Consts.settingsPage:
            return _buildRoute(settings, const SettingsPage());

          case Consts.homePage:
          case '':
            return _buildRoute(settings, const HomePage());
        }
        return null;
      },

      // start
      initialRoute: '/',
      title: t.app,
      restorationScopeId: 'root',
      debugShowCheckedModeBanner: false,
    );
  }

  PageTransition<dynamic> _buildRoute(RouteSettings? settings, Widget child) {
    const transition = PageTransitionType.bottomToTop;
    return PageTransition(
      type: transition,
      settings: settings,
      child: child,
    );
  }

  // ----------------------------------------------------------------------

  void refresh() => setState(() {});
}
