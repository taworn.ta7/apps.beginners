class Consts {
  // top-level page constants
  static const homePage = '/';
  static const testPage = '/test';
  static const settingsPage = '/settings';
}
