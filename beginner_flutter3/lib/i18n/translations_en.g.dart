part of 'translations.g.dart';

// Path: <root>
class _TranslationsEn implements BaseTranslations<AppLocale, _TranslationsEn> {

	/// You can call this constructor and build your own translation instance of this locale.
	/// Constructing via the enum [AppLocale.build] is preferred.
	_TranslationsEn.build({Map<String, Node>? overrides, PluralResolver? cardinalResolver, PluralResolver? ordinalResolver})
		: assert(overrides == null, 'Set "translation_overrides: true" in order to enable this feature.'),
		  $meta = TranslationMetadata(
		    locale: AppLocale.en,
		    overrides: overrides ?? {},
		    cardinalResolver: cardinalResolver,
		    ordinalResolver: ordinalResolver,
		  ) {
		$meta.setFlatMapFunction(_flatMapFunction);
	}

	/// Metadata for the translations of <en>.
	@override final TranslationMetadata<AppLocale, _TranslationsEn> $meta;

	/// Access flat map
	dynamic operator[](String key) => $meta.getTranslation(key);

	late final _TranslationsEn _root = this; // ignore: unused_field

	// Translations
	late final _TranslationsMessageBoxEn messageBox = _TranslationsMessageBoxEn._(_root);
	late final _TranslationsStringsEn strings = _TranslationsStringsEn._(_root);
	late final _TranslationsWaitBoxEn waitBox = _TranslationsWaitBoxEn._(_root);
}

// Path: messageBox
class _TranslationsMessageBoxEn {
	_TranslationsMessageBoxEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get close => 'Close';
	String get ok => 'OK';
	String get cancel => 'Cancel';
	String get yes => 'Yes';
	String get no => 'No';
	String get retry => 'Retry';
	String get info => 'Information';
	String get warning => 'Warning';
	String get error => 'Error';
	String get question => 'Question';
}

// Path: strings
class _TranslationsStringsEn {
	_TranslationsStringsEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get app => 'Beginner App';
	late final _TranslationsStringsLocaleEn locale = _TranslationsStringsLocaleEn._(_root);
	late final _TranslationsStringsModeEn mode = _TranslationsStringsModeEn._(_root);
	late final _TranslationsStringsPagesEn pages = _TranslationsStringsPagesEn._(_root);
	late final _TranslationsStringsHomePageEn homePage = _TranslationsStringsHomePageEn._(_root);
	late final _TranslationsStringsTestPageEn testPage = _TranslationsStringsTestPageEn._(_root);
	late final _TranslationsStringsSettingsPageEn settingsPage = _TranslationsStringsSettingsPageEn._(_root);
}

// Path: waitBox
class _TranslationsWaitBoxEn {
	_TranslationsWaitBoxEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get message => 'Please wait...';
}

// Path: strings.locale
class _TranslationsStringsLocaleEn {
	_TranslationsStringsLocaleEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get en => 'English';
	String get th => 'Thai';
}

// Path: strings.mode
class _TranslationsStringsModeEn {
	_TranslationsStringsModeEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get default_ => 'Default';
	String get light => 'Light';
	String get dark => 'Dark';
}

// Path: strings.pages
class _TranslationsStringsPagesEn {
	_TranslationsStringsPagesEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get home => 'Home';
	String get test => 'Test';
	String get settings => 'Settings';
}

// Path: strings.homePage
class _TranslationsStringsHomePageEn {
	_TranslationsStringsHomePageEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get title => 'Beginner App';
	String get hello => 'Hello, world ^-^';
}

// Path: strings.testPage
class _TranslationsStringsTestPageEn {
	_TranslationsStringsTestPageEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get title => 'Testing';
	String get messageBox => 'Message Box';
	String get close => 'Close';
	String get ok => 'OK';
	String get okCancel => 'OK/Cancel';
	String get yesNo => 'Yes/No';
	String get retryCancel => 'Retry/Cancel';
	String get info => 'Information';
	String get warning => 'Warning';
	String get error => 'Error';
	String get question => 'Question';
	String get content => 'Aaaaaa Bbbbbb Cccccc Dddddd Eeeeee Ffffff Aaaaaa Bbbbbb Cccccc Dddddd Eeeeee Ffffff Aaaaaa Bbbbbb Cccccc Dddddd Eeeeee Ffffff.\n';
	String get waitBox => 'Wait Box';
	String get wait => 'Please wait...';
}

// Path: strings.settingsPage
class _TranslationsStringsSettingsPageEn {
	_TranslationsStringsSettingsPageEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get title => 'Settings';
	String get theme => 'Theme Selection';
	List<String> get colors => [
		'Indigo',
		'Light Blue',
		'Teal',
		'Green',
		'Amber',
		'Deep Orange',
		'Red',
		'Pink',
		'Purple',
		'Deep Purple',
	];
}
