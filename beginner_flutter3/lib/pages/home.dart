import 'package:logging/logging.dart';
import 'package:flutter/material.dart';
import '../i18n/translations.g.dart';
import '../ui/custom_app_bar.dart';
import '../ui/custom_drawer.dart';

/// HomePage class.
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

// ----------------------------------------------------------------------

class _HomePageState extends State<HomePage> with RestorationMixin {
  static final log = Logger('HomePage');

  @override
  void initState() {
    super.initState();
    log.fine("$this initState()");
  }

  @override
  void dispose() {
    log.fine("$this dispose()");
    super.dispose();
  }

  @override
  String? get restorationId => 'HomePage';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {}

  // ----------------------------------------------------------------------

  /// Build widget tree.
  @override
  Widget build(BuildContext context) {
    final t = Translations.of(context).strings;

    return Scaffold(
      // AppBar
      appBar: CustomAppBar(
        title: t.homePage.title,
      ),

      // Drawer
      drawer: const CustomDrawer(),

      // body
      body: Center(
        child: Text(t.homePage.hello),
      ),
    );
  }
}
