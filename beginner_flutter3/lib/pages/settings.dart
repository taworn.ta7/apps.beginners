import 'package:logging/logging.dart';
import 'package:flutter/material.dart';
import '../i18n/translations.g.dart';
import '../ui/custom_app_bar.dart';
import '../ui/custom_drawer.dart';
import '../theme_manager.dart';

/// SettingsPage class.
class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

// ----------------------------------------------------------------------

class _SettingsPageState extends State<SettingsPage> with RestorationMixin {
  static final log = Logger('SettingsPage');

  @override
  void initState() {
    super.initState();
    log.fine("$this initState()");
  }

  @override
  void dispose() {
    log.fine("$this dispose()");
    super.dispose();
  }

  @override
  String? get restorationId => 'SettingsPage';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {}

  // ----------------------------------------------------------------------

  /// Build widget tree.
  @override
  Widget build(BuildContext context) {
    final t = Translations.of(context).strings;
    final size = MediaQuery.of(context).size;
    final double itemWidth = size.width / 2;
    const double itemHeight = 48;

    return Scaffold(
      // AppBar
      appBar: CustomAppBar(
        title: t.settingsPage.title,
      ),

      // Drawer
      drawer: const CustomDrawer(),

      // body
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(32),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // theme selection header
              Text(
                t.settingsPage.theme,
                style:
                    const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // theme selection grid
              GridView.count(
                primary: false,
                shrinkWrap: true,
                childAspectRatio: (itemWidth / itemHeight),
                crossAxisSpacing: 8,
                mainAxisSpacing: 8,
                crossAxisCount: 2,
                children: List.generate(ThemeManager.list.length, (index) {
                  return ListTile(
                    title: Text(t.settingsPage.colors[index]),
                    leading: const Icon(Icons.circle),
                    iconColor: ThemeManager.list[index],
                    onTap: () => ThemeManager.instance().change(context, index),
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
