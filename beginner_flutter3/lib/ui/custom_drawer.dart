//import 'package:logging/logging.dart';
import 'package:flutter/material.dart';
import '../i18n/translations.g.dart';
import '../consts.dart';

/// A customized Drawer.
class CustomDrawer extends StatefulWidget {
  const CustomDrawer({super.key});

  @override
  State<CustomDrawer> createState() => _CustomDrawerState();
}

// ----------------------------------------------------------------------

class _CustomDrawerState extends State<CustomDrawer> {
  //static final log = Logger('CustomDrawer');

  // ----------------------------------------------------------------------

  /// Build widget tree.
  @override
  Widget build(BuildContext context) {
    final t = Translations.of(context).strings;

    return Drawer(
      child: ListView(
        padding: const EdgeInsets.all(16),
        children: [
          // home
          ListTile(
            title: Text(t.pages.home),
            leading: const Icon(Icons.home),
            onTap: () {
              Navigator.pop(context);
              Navigator.restorablePopAndPushNamed(
                context,
                Consts.homePage,
              );
            },
          ),

          // test
          ListTile(
            title: Text(t.pages.test),
            leading: const Icon(Icons.bug_report),
            onTap: () {
              Navigator.pop(context);
              Navigator.restorablePopAndPushNamed(
                context,
                Consts.testPage,
              );
            },
          ),

          // settings
          ListTile(
            title: Text(t.pages.settings),
            leading: const Icon(Icons.settings),
            onTap: () {
              Navigator.pop(context);
              Navigator.restorablePopAndPushNamed(
                context,
                Consts.settingsPage,
              );
            },
          ),
        ],
      ),
    );
  }
}
