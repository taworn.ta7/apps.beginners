import 'package:flutter/material.dart';
import '../i18n/translations.g.dart';

enum MessageBoxType {
  close,
  ok,
  okCancel,
  yesNo,
  retryCancel,
}

/// Options for MessageBox class.
class MessageBoxOptions {
  MessageBoxType type;
  Color titleColor;
  bool button0Negative;
  bool button1Negative;
  bool barrierDismissible;

  MessageBoxOptions({
    this.type = MessageBoxType.close,
    this.titleColor = Colors.grey,
    this.button0Negative = false,
    this.button1Negative = false,
    this.barrierDismissible = false,
  });
}

/// Generic message dialog box.
class MessageBox {
  static Future<bool?> show({
    required BuildContext context,
    required String message,
    required String caption,
    required MessageBoxOptions options,
  }) {
    return showDialog<bool>(
      context: context,
      barrierDismissible: options.barrierDismissible,
      builder: (BuildContext context) => AlertDialog(
        title: Text(
          caption,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: options.titleColor,
          ),
        ),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(message),
          ],
        ),
        actions: _buildButtons(context, options),
      ),
    );
  }

  static List<Widget> _buildButtons(
    BuildContext context,
    MessageBoxOptions options,
  ) {
    // expand type to button0 and/or button1
    final t = Translations.of(context).messageBox;
    Icon icon0;
    Icon? icon1;
    String button0;
    String? button1;
    switch (options.type) {
      case MessageBoxType.close:
        icon0 = const Icon(Icons.close);
        button0 = t.close;
        break;
      case MessageBoxType.ok:
        icon0 = const Icon(Icons.check_circle);
        button0 = t.ok;
        break;
      case MessageBoxType.okCancel:
        icon0 = const Icon(Icons.check_circle);
        button0 = t.ok;
        icon1 = const Icon(Icons.cancel);
        button1 = t.cancel;
        break;
      case MessageBoxType.yesNo:
        icon0 = const Icon(Icons.done);
        button0 = t.yes;
        icon1 = const Icon(Icons.cancel);
        button1 = t.no;
        break;
      case MessageBoxType.retryCancel:
        icon0 = const Icon(Icons.autorenew);
        button0 = t.retry;
        icon1 = const Icon(Icons.cancel);
        button1 = t.cancel;
        break;
    }

    // build button list
    if (button1 != null) {
      return <Widget>[
        TextButton.icon(
          style: TextButton.styleFrom(
            foregroundColor: options.button1Negative ? Colors.red : Colors.blue,
          ),
          icon: icon1!,
          label: Text(button1),
          onPressed: () => Navigator.pop(context, false),
        ),
        TextButton.icon(
          style: TextButton.styleFrom(
            foregroundColor: options.button0Negative ? Colors.red : Colors.blue,
          ),
          icon: icon0,
          label: Text(button0),
          onPressed: () => Navigator.pop(context, true),
        ),
      ];
    } else {
      return <Widget>[
        TextButton.icon(
          style: TextButton.styleFrom(
            foregroundColor: options.button0Negative ? Colors.red : Colors.blue,
          ),
          icon: icon0,
          label: Text(button0),
          onPressed: () => Navigator.pop(context, false),
        ),
      ];
    }
  }

  // ----------------------------------------------------------------------

  /// Show generic information.
  static Future<bool?> info(
    BuildContext context,
    String message, {
    bool? button0Negative,
  }) {
    final t = Translations.of(context).messageBox;
    return show(
      context: context,
      message: message,
      caption: t.info,
      options: MessageBoxOptions(
        type: MessageBoxType.close,
        titleColor: Colors.green,
        barrierDismissible: true,
        button0Negative: button0Negative ?? false,
      ),
    );
  }

  /// Show warning.
  static Future<bool?> warning(
    BuildContext context,
    String message, {
    bool? button0Negative,
  }) {
    final t = Translations.of(context).messageBox;
    return show(
      context: context,
      message: message,
      caption: t.warning,
      options: MessageBoxOptions(
        type: MessageBoxType.close,
        titleColor: Colors.orange,
        barrierDismissible: true,
        button0Negative: button0Negative ?? false,
      ),
    );
  }

  /// Show error.
  static Future<bool?> error(
    BuildContext context,
    String message, {
    bool? button0Negative,
  }) {
    final t = Translations.of(context).messageBox;
    return show(
      context: context,
      message: message,
      caption: t.error,
      options: MessageBoxOptions(
        type: MessageBoxType.close,
        titleColor: Colors.red,
        barrierDismissible: false,
        button0Negative: button0Negative ?? false,
      ),
    );
  }

  /// Show question.
  static Future<bool?> question(
    BuildContext context,
    String message, {
    bool? button0Negative,
    bool? button1Negative,
  }) {
    final t = Translations.of(context).messageBox;
    return show(
      context: context,
      message: message,
      caption: t.question,
      options: MessageBoxOptions(
        type: MessageBoxType.yesNo,
        titleColor: Colors.blue,
        barrierDismissible: false,
        button0Negative: button0Negative ?? false,
        button1Negative: button1Negative ?? false,
      ),
    );
  }
}
