package io.oo.beginner.pages

import android.annotation.SuppressLint
import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import io.oo.beginner.R
import io.oo.beginner.ui.AppViewModel
import io.oo.beginner.ui.ComposeAppBar
import io.oo.beginner.ui.DataStoreSettings
import io.oo.beginner.ui.ThemeColor
import kotlinx.coroutines.runBlocking

private const val TAG = "SettingsPage"

private fun onBack(navController: NavHostController) {
    Log.i(TAG, "leave $TAG")
    navController.popBackStack()
}

/**
 * Composing settings page.
 */
@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ComposeSettingsPage(
    viewModel: AppViewModel,
    navController: NavHostController,
) {
    BackHandler(true) {
        Log.d(TAG, "default back button clicked")
        onBack(navController)
    }

    Scaffold(
        topBar = {
            ComposeAppBar(
                viewModel = viewModel,
                navController = navController,
                title = stringResource(R.string.settings_title),
                onBack = {
                    Log.d(TAG, "back button, on left-top, clicked")
                    onBack(navController)
                },
            )
        },
    ) {
        Box(
            modifier = Modifier.padding(it),
        ) {
            ComposeContent(
                viewModel = viewModel,
                navController = rememberNavController(),
            )
        }
    }
}

// ----------------------------------------------------------------------

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun ComposeContent(
    viewModel: AppViewModel,
    navController: NavHostController,
) {
    val dataStore = DataStoreSettings(LocalContext.current)
    val list: List<ThemeColorItem> = listOf(
        ThemeColorItem(ThemeColor.INDIGO, R.string.settings_indigo, Color(0xFF4355B9)),
        ThemeColorItem(ThemeColor.RED, R.string.settings_red, Color(0xFFBB1614)),
        ThemeColorItem(ThemeColor.GREEN, R.string.settings_green, Color(0xFF006E1C)),
        ThemeColorItem(ThemeColor.YELLOW, R.string.settings_yellow, Color(0xFF695F00)),
    )

    Box(
        modifier = Modifier.padding(32.dp),
    ) {
        Column(
            horizontalAlignment = Alignment.Start, modifier = Modifier
                .fillMaxSize()
                .fillMaxWidth()
        ) {
            Text(
                stringResource(R.string.settings_theme_color),
                fontSize = 30.sp,
                modifier = Modifier.padding(bottom = 8.dp),
            )

            LazyVerticalGrid(
                columns = GridCells.Adaptive(minSize = 128.dp),
            ) {
                items(list.size) {
                    val item = list[it]
                    Surface(
                        onClick = {
                            Log.i(
                                TAG, String.format(
                                    "change theme from %s to %s",
                                    viewModel.themeColor.value,
                                    item.themeColor
                                )
                            )
                            viewModel.setThemeColor(item.themeColor)
                            runBlocking {
                                viewModel.saveSettings(dataStore)
                            }
                        },
                    ) {
                        Row(
                            horizontalArrangement = Arrangement.Start,
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(16.dp),
                        ) {
                            Icon(
                                imageVector = Icons.Default.Favorite,
                                contentDescription = stringResource(item.themeName),
                                tint = item.tint,
                                modifier = Modifier.size(24.dp),
                            )
                            Spacer(modifier = Modifier.width(16.dp))
                            Text(
                                text = stringResource(item.themeName),
                                style = MaterialTheme.typography.bodyMedium,
                                textAlign = TextAlign.Center,
                            )
                        }
                    }
                }
            }
        }
    }
}

data class ThemeColorItem(
    val themeColor: ThemeColor,
    val themeName: Int,
    val tint: Color,
)


// ----------------------------------------------------------------------

//
// Previews
//

@Preview(showBackground = true)
@Composable
private fun PreviewComposeSettingsPage() {
    ComposeSettingsPage(
        viewModel = AppViewModel(),
        navController = rememberNavController(),
    )
}
