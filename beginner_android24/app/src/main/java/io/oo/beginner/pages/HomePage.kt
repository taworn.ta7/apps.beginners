package io.oo.beginner.pages

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import io.oo.beginner.Pages
import io.oo.beginner.R
import io.oo.beginner.ui.AppViewModel
import io.oo.beginner.ui.ComposeAppBar
import kotlinx.coroutines.launch

private const val TAG = "HomePage"

/**
 * Composing home page.
 */
@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ComposeHomePage(
    viewModel: AppViewModel,
    navController: NavHostController,
) {
    val coroutineScope = rememberCoroutineScope()
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)

    ModalNavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                ) {
                    ComposeMenuItem(
                        text = stringResource(R.string.menu_test),
                        icon = Icons.Default.CheckCircle,
                        onClick = {
                            navController.navigate(Pages.testPage.route)
                            coroutineScope.launch {
                                drawerState.close()
                            }
                        },
                    )
                    ComposeMenuItem(
                        text = stringResource(R.string.menu_settings),
                        icon = Icons.Default.Settings,
                        onClick = {
                            navController.navigate(Pages.settingsPage.route)
                            coroutineScope.launch {
                                drawerState.close()
                            }
                        },
                    )
                }
            }
        },
    ) {
        Scaffold(
            topBar = {
                ComposeAppBar(
                    viewModel = viewModel,
                    navController = navController,
                    title = stringResource(R.string.home_title),
                    onDrawer = {
                        coroutineScope.launch {
                            drawerState.open()
                        }
                    },
                )
            },
        ) {
            Box(
                modifier = Modifier.padding(it),
            ) {
                ComposeContent(
                    viewModel = viewModel,
                    navController = rememberNavController(),
                )
            }
        }
    }
}

// ----------------------------------------------------------------------

@ExperimentalMaterial3Api
@Composable
private fun ComposeContent(
    viewModel: AppViewModel,
    navController: NavHostController,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxSize(),
    ) {
        Box(
            modifier = Modifier
                .border(
                    width = 4.dp,
                    color = MaterialTheme.colorScheme.onTertiary,
                    shape = RoundedCornerShape(32.dp),
                )
                .clip(
                    shape = RoundedCornerShape(32.dp),
                ),
        ) {
            Box(
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.tertiary)
                    .padding(32.dp)
            ) {
                Text(
                    text = stringResource(R.string.home_hello),
                    color = MaterialTheme.colorScheme.onTertiary,
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun ComposeMenuItem(
    text: String,
    icon: ImageVector,
    onClick: () -> Unit,
) {
    Surface(onClick = onClick) {
        Row(
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
        ) {
            Icon(
                imageVector = icon,
                contentDescription = text,
                modifier = Modifier.size(24.dp),
            )
            Spacer(modifier = Modifier.width(16.dp))
            Text(
                text = text,
                style = MaterialTheme.typography.bodyMedium,
                textAlign = TextAlign.Center,
            )
        }
    }
}

// ----------------------------------------------------------------------

//
// Previews
//

@Preview(showBackground = true)
@Composable
private fun PreviewComposeHomePage() {
    ComposeHomePage(
        viewModel = AppViewModel(),
        navController = rememberNavController(),
    )
}
