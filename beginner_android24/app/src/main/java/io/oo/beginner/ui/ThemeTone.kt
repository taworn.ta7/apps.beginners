package io.oo.beginner.ui

enum class ThemeTone(val value: Int) {
    AUTO(0), LIGHT(1), NIGHT(2);

    companion object {
        fun fromInt(value: Int) = ThemeTone.values().first { it.value == value }
    }
}
