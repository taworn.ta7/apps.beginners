package io.oo.beginner.pages

import android.annotation.SuppressLint
import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.outlined.Close
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import io.oo.beginner.R
import io.oo.beginner.ui.AppViewModel
import io.oo.beginner.ui.ComposeAppBar
import io.oo.beginner.widgets.ComposeErrorBox
import io.oo.beginner.widgets.ComposeInfoBox
import io.oo.beginner.widgets.ComposeMessageBox
import io.oo.beginner.widgets.ComposeMessageBoxButton
import io.oo.beginner.widgets.ComposeQuestionBox
import io.oo.beginner.widgets.ComposeWaitBox
import io.oo.beginner.widgets.ComposeWarningBox
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.Timer
import kotlin.concurrent.schedule

private const val TAG = "TestPage"

private fun onBack(navController: NavHostController) {
    Log.i(TAG, "leave $TAG")
    navController.popBackStack()
}

/**
 * Composing test page.
 */
@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ComposeTestPage(
    viewModel: AppViewModel,
    navController: NavHostController,
) {
    BackHandler(true) {
        Log.d(TAG, "default back button clicked")
        onBack(navController)
    }

    Scaffold(
        topBar = {
            ComposeAppBar(
                viewModel = viewModel,
                navController = navController,
                title = stringResource(R.string.test_title),
                onBack = {
                    Log.d(TAG, "back button, on left-top, clicked")
                    onBack(navController)
                },
            )
        },
    ) {
        Box(
            modifier = Modifier.padding(it),
        ) {
            ComposeContent(
                viewModel = viewModel,
                navController = rememberNavController(),
            )
        }
    }
}

// ----------------------------------------------------------------------

@Composable
private fun ComposeContent(
    viewModel: AppViewModel,
    navController: NavHostController,
) {
    val stateVertical = rememberScrollState(0)

    Box(
        modifier = Modifier.padding(32.dp),
    ) {
        Column(
            horizontalAlignment = Alignment.Start,
            modifier = Modifier
                .fillMaxSize()
                .fillMaxWidth()
                .verticalScroll(stateVertical),
        ) {
            //
            // Test MessageBox
            //

            Text(
                stringResource(R.string.test_test_message_box),
                fontSize = 30.sp,
                modifier = Modifier.padding(bottom = 8.dp),
            )

            // ----------------------------------------------------------------------

            var openCustomBox by remember { mutableStateOf(false) }
            ComposeButton(
                stringResource(R.string.test_test_message_box_custom_box),
                onClick = { openCustomBox = true },
            )
            when {
                openCustomBox -> ComposeMessageBox(
                    icon = Icons.Default.Favorite,
                    caption = stringResource(R.string.test_test_message_box_caption),
                    content = stringResource(R.string.test_test_message_box_content),
                    onDismissRequest = {
                        openCustomBox = false
                        Log.d(TAG, "message box dismissed out of box :|")
                    },
                    dismissButton = {
                        ComposeMessageBoxButton(
                            text = stringResource(R.string.test_test_message_box_dismiss),
                            icon = Icons.Outlined.Close,
                            iconDescription = null,
                            iconTint = Color(0xffcc0000),
                            onClick = {
                                openCustomBox = false
                                Log.d(TAG, "message box dismissed :(")
                            },
                        )
                    },
                    confirmButton = {
                        ComposeMessageBoxButton(
                            text = stringResource(R.string.test_test_message_box_confirm),
                            icon = Icons.Filled.Check,
                            iconDescription = "Confirm here",
                            iconTint = Color(0xff00cc00),
                            onClick = {
                                openCustomBox = false
                                Log.d(TAG, "message box confirmed :)")
                            },
                        )
                    },
                )
            }

            // ----------------------------------------------------------------------

            var openInfoBox by remember { mutableStateOf(false) }
            ComposeButton(
                stringResource(R.string.test_test_message_box_info_box),
                onClick = { openInfoBox = true },
            )
            when {
                openInfoBox -> ComposeInfoBox(
                    caption = stringResource(R.string.test_test_message_box_caption),
                    content = stringResource(R.string.test_test_message_box_content),
                    onClosed = { result: Boolean ->
                        openInfoBox = false
                        Log.d(TAG, "info box, result: $result")
                    },
                )
            }

            // ----------------------------------------------------------------------

            var openWarningBox by remember { mutableStateOf(false) }
            ComposeButton(
                stringResource(R.string.test_test_message_box_warning_box),
                onClick = { openWarningBox = true },
            )
            when {
                openWarningBox -> ComposeWarningBox(
                    caption = stringResource(R.string.test_test_message_box_caption),
                    content = stringResource(R.string.test_test_message_box_content),
                    onClosed = { result: Boolean ->
                        openWarningBox = false
                        Log.d(TAG, "warning box, result: $result")
                    },
                )
            }

            // ----------------------------------------------------------------------

            var openErrorBox by remember { mutableStateOf(false) }
            ComposeButton(
                stringResource(R.string.test_test_message_box_error_box),
                onClick = { openErrorBox = true },
            )
            when {
                openErrorBox -> ComposeErrorBox(
                    caption = stringResource(R.string.test_test_message_box_caption),
                    content = stringResource(R.string.test_test_message_box_content),
                    onClosed = { result: Boolean ->
                        openErrorBox = false
                        Log.d(TAG, "error box, result $result")
                    },
                )
            }

            // ----------------------------------------------------------------------

            var openQuestionBox by remember { mutableStateOf(false) }
            ComposeButton(
                stringResource(R.string.test_test_message_box_question_box),
                onClick = { openQuestionBox = true },
            )
            when {
                openQuestionBox -> ComposeQuestionBox(
                    caption = stringResource(R.string.test_test_message_box_caption),
                    content = stringResource(R.string.test_test_message_box_content),
                    onClosed = { result: Boolean ->
                        openQuestionBox = false
                        Log.d(TAG, "question box, result: $result")
                    },
                )
            }

            // ----------------------------------------------------------------------

            //
            // Test WaitBox
            //

            Text(
                stringResource(R.string.test_test_wait_box),
                fontSize = 30.sp,
                modifier = Modifier.padding(top = 32.dp, bottom = 8.dp),
            )

            // ----------------------------------------------------------------------

            var openWaitBox by remember { mutableStateOf(false) }
            ComposeButton(
                stringResource(R.string.test_test_wait_box),
                onClick = {
                    Log.d(TAG, "wait box, opening")
                    openWaitBox = true
                },
            )
            if (openWaitBox) {
                ComposeWaitBox(
                )
                Timer().schedule(0) {
                    runBlocking {
                        launch {
                            delay(5000)
                            openWaitBox = false
                            Log.d(TAG, "wait box, closed")
                        }
                    }
                }
            }

        }
    }
}

@Composable
private fun ComposeButton(
    text: String,
    onClick: () -> Unit,
) {
    Button(
        modifier = Modifier.fillMaxWidth(),
        onClick = onClick,
    ) {
        Text(text)
    }
}


// ----------------------------------------------------------------------

//
// Previews
//

@Preview(showBackground = true)
@Composable
private fun PreviewComposeTestPage() {
    ComposeTestPage(
        viewModel = AppViewModel(),
        navController = rememberNavController(),
    )
}
