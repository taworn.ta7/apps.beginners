package io.oo.beginner.widgets

import android.content.res.Configuration.UI_MODE_NIGHT_NO
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.util.Log
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material.icons.outlined.Close
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.oo.beginner.R

private const val TAG = "ComposeMessageBox"

/**
 * Composing MessageBox.
 */
@Composable
fun ComposeMessageBox(
    icon: ImageVector?,
    iconTint: Color = MaterialTheme.colorScheme.primary,
    caption: String? = null,
    content: String,
    onDismissRequest: () -> Unit,
    dismissButton: (@Composable () -> Unit)? = null,
    confirmButton: (@Composable () -> Unit)? = null,
) {
    AlertDialog(
        icon = {
            if (icon != null) {
                Icon(
                    icon,
                    contentDescription = null,
                    tint = iconTint,
                    modifier = Modifier.size(64.dp),
                )
            } else {
                Unit
            }
        },
        title = {
            if (caption != null) Text(caption)
        },
        text = {
            Text(content)
        },
        onDismissRequest = onDismissRequest,
        dismissButton = dismissButton,
        confirmButton = confirmButton ?: {},
    )
}

/**
 * Composing MessageBox's button.
 */
@Composable
fun ComposeMessageBoxButton(
    text: String,
    icon: ImageVector,
    iconDescription: String? = null,
    iconTint: Color = MaterialTheme.colorScheme.primary,
    onClick: () -> Unit,
) {
    TextButton(onClick = onClick) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Icon(
                imageVector = icon,
                contentDescription = iconDescription,
                tint = iconTint,
                modifier = Modifier
                    .size(32.dp)
                    .padding(PaddingValues(end = 4.dp)),
            )
            Text(text)
        }
    }
}

// ----------------------------------------------------------------------

/**
 * Composes information message box.
 * For inform user about general news.
 */
@Composable
fun ComposeInfoBox(
    caption: String? = null,
    content: String,
    onClosed: (Boolean) -> Unit,
) {
    ComposeMessageBox(
        icon = Icons.Default.Info,
        iconTint = Color(0xFF00C853),
        caption = caption,
        content = content,
        onDismissRequest = {
            Log.d(TAG, "info box closed, out of box")
            onClosed(false)
        },
        dismissButton = {
            ComposeMessageBoxButton(
                text = stringResource(R.string.message_box_close),
                icon = Icons.Default.Close,
                iconDescription = stringResource(R.string.message_box_close),
                iconTint = Color(0xFF00C853),
                onClick = {
                    Log.d(TAG, "info box closed")
                    onClosed(false)
                },
            )
        },
    )
}

/**
 * Composes warning message box.
 * For inform user about warning, but it can be fix.
 */
@Composable
fun ComposeWarningBox(
    caption: String? = null,
    content: String,
    onClosed: (result: Boolean) -> Unit,
) {
    ComposeMessageBox(
        icon = Icons.Default.Warning,
        iconTint = Color(0xFFFF6D00),
        caption = caption,
        content = content,
        onDismissRequest = {
            Log.d(TAG, "warning box closed, out of box")
            onClosed(false)
        },
        dismissButton = {
            ComposeMessageBoxButton(
                text = stringResource(R.string.message_box_close),
                icon = Icons.Default.Close,
                iconDescription = stringResource(R.string.message_box_close),
                iconTint = Color(0xFFFF6D00),
                onClick = {
                    Log.d(TAG, "warning box closed")
                    onClosed(false)
                },
            )
        },
    )
}

/**
 * Composes error message box.
 * For inform user about severity error.
 */
@Composable
fun ComposeErrorBox(
    caption: String? = null,
    content: String,
    onClosed: (result: Boolean) -> Unit,
) {
    ComposeMessageBox(
        icon = Icons.Default.Warning,
        iconTint = Color(0xFFD50000),
        caption = caption,
        content = content,
        onDismissRequest = {
            Log.d(TAG, "error box closed, out of box")
            onClosed(false)
        },
        dismissButton = {
            ComposeMessageBoxButton(
                text = stringResource(R.string.message_box_close),
                icon = Icons.Default.Close,
                iconDescription = stringResource(R.string.message_box_close),
                iconTint = Color(0xFFD50000),
                onClick = {
                    Log.d(TAG, "error box closed")
                    onClosed(false)
                },
            )
        },
    )
}

/**
 * Composes question message box.
 * For question user.
 */
@Composable
fun ComposeQuestionBox(
    caption: String? = null,
    content: String,
    onClosed: (result: Boolean) -> Unit,
) {
    ComposeMessageBox(
        icon = Icons.Default.Info,
        iconTint = Color(0xFF2962FF),
        caption = caption,
        content = content,
        onDismissRequest = {
            Log.d(TAG, "question box dismiss, out of box")
            onClosed(false)
        },
        dismissButton = {
            ComposeMessageBoxButton(
                text = stringResource(R.string.message_box_no),
                icon = Icons.Default.Close,
                iconDescription = stringResource(R.string.message_box_no),
                iconTint = Color(0xFFD50000),
                onClick = {
                    Log.d(TAG, "question box dismiss")
                    onClosed(false)
                },
            )
        },
        confirmButton = {
            ComposeMessageBoxButton(
                text = stringResource(R.string.message_box_yes),
                icon = Icons.Default.Check,
                iconDescription = stringResource(R.string.message_box_yes),
                iconTint = Color(0xFF2962FF),
                onClick = {
                    Log.d(TAG, "question box confirm")
                    onClosed(true)
                },
            )
        },
    )
}


// ----------------------------------------------------------------------

//
// Previews
//

@Preview("Light", showBackground = true, uiMode = UI_MODE_NIGHT_NO)
@Preview("Night", showBackground = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun PreviewComposeMessageBox() {
    ComposeMessageBox(
        icon = Icons.Default.Favorite,
        caption = "Alert dialog example",
        content = "This is an example of an alert dialog with buttons.",
        onDismissRequest = {
            Log.d(TAG, "message box dismissed out of box :|")
        },
        dismissButton = {
            ComposeMessageBoxButton(
                text = "Dismiss",
                icon = Icons.Outlined.Close,
                iconDescription = null,
                iconTint = Color(0xFFCC0000),
                onClick = {
                    Log.d(TAG, "message box dismissed :(")
                },
            )
        },
        confirmButton = {
            ComposeMessageBoxButton(
                text = "Confirm",
                icon = Icons.Filled.Check,
                iconDescription = "Confirm here",
                iconTint = Color(0xFF00CC00),
                onClick = {
                    Log.d(TAG, "message box confirmed :)")
                },
            )
        },
    )
}

@Preview("Light", showBackground = true, uiMode = UI_MODE_NIGHT_NO)
@Preview("Night", showBackground = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun PreviewComposeInfoBox() {
    ComposeInfoBox(
        caption = "Caption",
        content = "An information to tell user.",
        onClosed = { result: Boolean ->
            Log.d(TAG, "info box, result: $result")
        },
    )
}

@Preview("Light", showBackground = true, uiMode = UI_MODE_NIGHT_NO)
@Preview("Night", showBackground = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun PreviewComposeWarningBox() {
    ComposeWarningBox(
        caption = "Caption",
        content = "A warning to tell user.",
        onClosed = { result: Boolean ->
            Log.d(TAG, "warning box, result: $result")
        },
    )
}

@Preview("Light", showBackground = true, uiMode = UI_MODE_NIGHT_NO)
@Preview("Night", showBackground = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun PreviewComposeErrorBox() {
    ComposeErrorBox(
        caption = "Caption",
        content = "An error to tell user.",
        onClosed = { result: Boolean ->
            Log.d(TAG, "error box, result: $result")
        },
    )
}

@Preview("Light", showBackground = true, uiMode = UI_MODE_NIGHT_NO)
@Preview("Night", showBackground = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun PreviewComposeQuestionBox() {
    ComposeQuestionBox(
        caption = "Caption",
        content = "A question to ask user?",
        onClosed = { result: Boolean ->
            Log.d(TAG, "question box, result: $result")
        },
    )
}
