package io.oo.beginner

import androidx.navigation.NamedNavArgument

data class NavigationCommand(
    val args: List<NamedNavArgument>,
    val route: String,
)

object Pages {

    val homePage = NavigationCommand(
        args = emptyList(),
        route = "HomePage",
    )

    val testPage = NavigationCommand(
        args = emptyList(),
        route = "TestPage",
    )

    val settingsPage = NavigationCommand(
        args = emptyList(),
        route = "SettingsPage",
    )

}
