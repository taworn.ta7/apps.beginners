package io.oo.beginner.widgets

import android.content.res.Configuration.UI_MODE_NIGHT_NO
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import io.oo.beginner.R

private const val TAG = "ComposeWaitBox"

/**
 * Composing WaitBox.
 */
@Composable
fun ComposeWaitBox(
) {
    Dialog(
        onDismissRequest = { },
        properties = DialogProperties(
            dismissOnBackPress = false,
            dismissOnClickOutside = false,
        ),
        content = {
            Box(
                modifier = Modifier
                    .background(Color.Transparent)
                    .border(
                        width = 4.dp,
                        color = MaterialTheme.colorScheme.error,
                        shape = RoundedCornerShape(32.dp),
                    )
                    .clip(
                        shape = RoundedCornerShape(32.dp),
                    ),
            ) {
                Box(
                    modifier = Modifier.background(MaterialTheme.colorScheme.errorContainer),
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier.padding(PaddingValues(32.dp)),
                    ) {
                        CircularProgressIndicator(
                            color = MaterialTheme.colorScheme.error,
                            strokeWidth = 8.dp,
                            modifier = Modifier.size(80.dp),
                        )
                        Spacer(modifier = Modifier.size(16.dp))
                        Text(stringResource(R.string.wait_box_message))
                        Text(stringResource(R.string.wait_box_message_2))
                    }
                }
            }
        },
    )
}


// ----------------------------------------------------------------------

//
// Previews
//

@Preview("Light", showBackground = true, uiMode = UI_MODE_NIGHT_NO)
@Preview("Night", showBackground = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun PreviewComposeWaitBox() {
    ComposeWaitBox()
}
