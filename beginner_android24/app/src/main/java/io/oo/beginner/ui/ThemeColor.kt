package io.oo.beginner.ui

enum class ThemeColor(val value: Int) {
    INDIGO(0), RED(1), GREEN(2), YELLOW(3);

    companion object {
        fun fromInt(value: Int) = ThemeColor.values().first { it.value == value }
    }
}
