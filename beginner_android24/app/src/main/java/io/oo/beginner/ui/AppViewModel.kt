package io.oo.beginner.ui

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel

private const val TAG = "AppViewModel"

class AppViewModel : ViewModel() {

    // ----------------------------------------------------------------------
    // Settings
    // ----------------------------------------------------------------------

    /**
     * Loads settings.
     */
    suspend fun loadSettings(dataStore: DataStoreSettings, next: () -> Unit) {
        val flow = dataStore.load()
        try {
            flow.collect {
                Log.d(TAG, "loaded settings: $it")
                loadNextStage(it, next)
            }
        } catch (e: Exception) {
            Log.d(TAG, "catch error, loaded default")
        }
        loadNextStage(Settings(), next)
    }

    private fun loadNextStage(settings: Settings, next: () -> Unit) {
        setThemeTone(ThemeTone.fromInt(settings.themeTone))
        setThemeColor(ThemeColor.fromInt(settings.themeColor))
        next()
    }

    /**
     * Saves settings.
     */
    suspend fun saveSettings(dataStore: DataStoreSettings) {
        dataStore.save(
            settings = Settings(
                themeTone = themeTone.value.value,
                themeColor = themeColor.value.value,
            )
        )
    }

    // ----------------------------------------------------------------------

    // track theme tone
    val themeTone: MutableState<ThemeTone> = mutableStateOf(ThemeTone.AUTO)

    // track theme color
    val themeColor: MutableState<ThemeColor> = mutableStateOf(ThemeColor.INDIGO)

    /**
     * Set theme tone.
     */
    fun setThemeTone(tone: ThemeTone) {
        themeTone.value = tone
    }

    /**
     * Set theme color.
     */
    fun setThemeColor(color: ThemeColor) {
        themeColor.value = color
    }

    // output string, for debugging
    override fun toString() = String.format(
        "AppViewModel(themeTone=%s, themeColor=%s)", themeTone.value, themeColor.value
    )

}
