package io.oo.beginner.ui

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.map

private const val TAG = "Settings"

data class Settings(
    var themeTone: Int = 0,
    var themeColor: Int = 0,
)

// ----------------------------------------------------------------------

private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

class DataStoreSettings(private val context: Context) {

    companion object {
        val THEME_TONE = intPreferencesKey("theme_tone")
        val THEME_COLOR = intPreferencesKey("theme_color")
    }

    fun load() = context.dataStore.data.map {
        Settings(
            themeTone = it[THEME_TONE] ?: 0,
            themeColor = it[THEME_COLOR] ?: 0,
        )
    }

    suspend fun save(settings: Settings) {
        context.dataStore.edit {
            it[THEME_TONE] = settings.themeTone
            it[THEME_COLOR] = settings.themeColor
        }
    }

    suspend fun clear() = context.dataStore.edit {
        it.clear()
    }

}
