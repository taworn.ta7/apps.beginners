package io.oo.beginner.ui

import android.util.Log
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import io.oo.beginner.R

private const val TAG = "ComposeAppBar"

/**
 * Composing AppBar.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ComposeAppBar(
    viewModel: AppViewModel,
    navController: NavController,
    title: String,
    onBack: (() -> Unit)? = null,
    onDrawer: (() -> Unit)? = null,
) {
    var expanded by remember { mutableStateOf(false) }

    TopAppBar(
        colors = TopAppBarDefaults.mediumTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer,
            titleContentColor = MaterialTheme.colorScheme.primary,
        ),

        title = {
            Text(title)
        },

        navigationIcon = {
            if (onBack != null) {
                // back button
                IconButton(onClick = onBack) {
                    Icon(Icons.Default.ArrowBack, stringResource(R.string.back))
                }
            } else if (onDrawer != null) {
                // back button
                IconButton(onClick = onDrawer) {
                    Icon(Icons.Default.Menu, stringResource(R.string.menu))
                }
            }
        },

        actions = {
            // pull-down menu
            IconButton(onClick = { expanded = true }) {
                Icon(Icons.Default.MoreVert, stringResource(R.string.profile_menu))
            }
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
            ) {
                ComposeProfileMenu(
                    viewModel = viewModel,
                    navController = navController,
                )
            }
        },
    )
}


// ----------------------------------------------------------------------

//
// Previews
//

@Preview
@Composable
private fun PreviewComposeAppBar() {
    ComposeAppBar(
        viewModel = AppViewModel(),
        navController = rememberNavController(),
        title = "Beginning",
        onBack = {
            Log.d(TAG, "back button clicked")
        },
    )
}
