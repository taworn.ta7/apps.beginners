package io.oo.beginner

import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import io.oo.beginner.pages.ComposeHomePage
import io.oo.beginner.pages.ComposeSettingsPage
import io.oo.beginner.pages.ComposeTestPage
import io.oo.beginner.ui.AppViewModel
import io.oo.beginner.ui.DataStoreSettings
import io.oo.beginner.ui.ThemeTone
import io.oo.beginner.ui.theme.AppTheme
import io.oo.beginner.widgets.ComposeWaitBox
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "beginning...")
        setContent {
            Composing()
        }
    }

    // ----------------------------------------------------------------------

    companion object {
        private const val TAG = "MainActivity"
    }

    private val viewModel: AppViewModel by viewModels()

    @Preview(showBackground = true)
    @Composable
    private fun PreviewComposing() {
        Composing()
    }

    @Composable
    private fun Composing() {
        val context = LocalContext.current
        val coScope = rememberCoroutineScope()
        var loaded by remember { mutableStateOf(false) }

        SideEffect {
            if (!loaded) {
                coScope.launch {
                    viewModel.loadSettings(DataStoreSettings(context), next = {
                        loaded = true
                    })
                }
            }
        }

        if (!loaded) {
            ComposeWaitBox(
            )
        } else {
            val dark = when (viewModel.themeTone.value) {
                ThemeTone.AUTO -> isSystemInDarkTheme()
                ThemeTone.LIGHT -> false
                ThemeTone.NIGHT -> true
            }

            AppTheme(
                darkTheme = dark,
                themeColor = viewModel.themeColor.value,
            ) {
                val navController = rememberNavController()
                NavHost(
                    navController = navController,
                    startDestination = Pages.homePage.route,
                ) {
                    composable(Pages.homePage.route) {
                        ComposeHomePage(
                            viewModel = viewModel,
                            navController = navController,
                        )
                    }
                    composable(Pages.testPage.route) {
                        ComposeTestPage(
                            viewModel = viewModel,
                            navController = navController,
                        )
                    }
                    composable(Pages.settingsPage.route) {
                        ComposeSettingsPage(
                            viewModel = viewModel,
                            navController = navController,
                        )
                    }
                }
            }
        }
    }

}
