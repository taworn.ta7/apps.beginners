package io.oo.beginner.ui

import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material.icons.twotone.Favorite
import androidx.compose.material3.Divider
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.os.LocaleListCompat
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import io.oo.beginner.R
import kotlinx.coroutines.runBlocking

private const val TAG = "ComposeProfileMenu"

/**
 * Composing profile menu.
 */
@Composable
fun ComposeProfileMenu(
    viewModel: AppViewModel,
    navController: NavController,
) {
    val dataStore = DataStoreSettings(LocalContext.current)

    // locale: default
    ComposeMenuItem(
        text = stringResource(R.string.profile_menu_locale_default),
        icon = Icons.TwoTone.Favorite,
        onClick = {
            Log.i(TAG, "change locale to default")
            AppCompatDelegate.setApplicationLocales(
                LocaleListCompat.forLanguageTags(""),
            )
        },
    )

    // locale: en
    ComposeMenuItem(
        text = stringResource(R.string.profile_menu_locale_en),
        icon = painterResource(R.drawable.locale_en),
        onClick = {
            Log.i(TAG, "change locale to en")
            AppCompatDelegate.setApplicationLocales(
                LocaleListCompat.forLanguageTags("en"),
            )
        },
    )

    // locale: th
    ComposeMenuItem(
        text = stringResource(R.string.profile_menu_locale_th),
        icon = painterResource(R.drawable.locale_th),
        onClick = {
            Log.i(TAG, "change locale to th")
            AppCompatDelegate.setApplicationLocales(
                LocaleListCompat.forLanguageTags("th"),
            )
        },
    )

    Divider()  // ----------------------------------------------------------------------

    // theme: default
    ComposeMenuItem(
        text = stringResource(R.string.profile_menu_theme_default),
        icon = Icons.TwoTone.Favorite,
        onClick = {
            setThemeTone(viewModel, dataStore, ThemeTone.AUTO)
        },
    )

    // theme: light
    ComposeMenuItem(
        text = stringResource(R.string.profile_menu_theme_light),
        icon = Icons.Outlined.FavoriteBorder,
        onClick = {
            setThemeTone(viewModel, dataStore, ThemeTone.LIGHT)
        },
    )

    // theme: dark
    ComposeMenuItem(
        text = stringResource(R.string.profile_menu_theme_dark),
        icon = Icons.Filled.Favorite,
        onClick = {
            setThemeTone(viewModel, dataStore, ThemeTone.NIGHT)
        },
    )
}

@Composable
private fun ComposeMenuItem(
    text: String,
    icon: ImageVector,
    onClick: () -> Unit,
) {
    DropdownMenuItem(
        leadingIcon = {
            Icon(
                icon,
                contentDescription = text,
            )
        },
        text = {
            Text(text)
        },
        onClick = onClick,
    )
}

@Composable
private fun ComposeMenuItem(
    text: String,
    icon: Painter,
    onClick: () -> Unit,
) {
    DropdownMenuItem(
        leadingIcon = {
            Icon(
                painter = icon,
                contentDescription = text,
                tint = Color.Unspecified,
                modifier = Modifier.size(24.dp),
            )
        },
        text = {
            Text(text)
        },
        onClick = onClick,
    )
}

private fun setThemeTone(viewModel: AppViewModel, dataStore: DataStoreSettings, tone: ThemeTone) {
    Log.i(TAG, String.format("change theme from %s to %s", viewModel.themeTone.value, tone))
    viewModel.setThemeTone(tone)
    runBlocking {
        viewModel.saveSettings(dataStore)
    }
}


// ----------------------------------------------------------------------

//
// Previews
//

@Preview(showBackground = true)
@Composable
private fun PreviewComposeProfileMenu() {
    ComposeProfileMenu(
        viewModel = AppViewModel(),
        navController = rememberNavController(),
    )
}
