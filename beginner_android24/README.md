# beginner_android24

Beginner code for Android API Level 24.

This program tests with emulator Pixel 3 API 30, Android 11.0 ("R"), x86.


## Features

* Light/Dark Mode
* Switchable 4 Color Themes
* 2 Languages, English and Thai
* MessageBox
* WaitBox


## Credits

Thank you, [Google Fonts](https://fonts.google.com/icons) API.

Thank you, [Melvin ilham Oktaviansyah](https://freeicons.io/profile/8939) on [freeicons.io](https://freeicons.io) for icons.

Thank you, [icon king1](https://freeicons.io/profile/3) on [freeicons.io](https://freeicons.io) for icons.


## Last

Sorry, but I'm not good at English and not good at coloring, too. T_T

