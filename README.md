# Beginners

Beginner codes for various platforms.


## Platforms

Currently, we have:

* [Beginner Android API Level 24](./beginner_android24)
* [Beginner Flutter 3](./beginner_flutter3)
* [Beginner Go 1](./beginner_go1)
* [Beginner Nest 10](./beginner_nest10)
* [Beginner Vue 3](./beginner_vue3)


## Last

Sorry, but I'm not good at English. T_T

