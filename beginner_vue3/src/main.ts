import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import './assets/main.css'
import router from './router'
import Breadcrumbs from './components/Breadcrumbs.vue'
import App from './App.vue'

// i18n
const i18n = createI18n({
	legacy: false,  // you must set to false, if you want to use Composition API
	locale: 'en',
	fallbackLocale: 'en',
	fallbackWarn: false,
	missingWarn: false,
	globalInjection: true,
	messages: {
		en: {
			'app': "Beginner",
			'path': {
				'home': "Home",
				'test-boxes': "Test Boxes",
				'test-boxes-shared': "Test Boxes Shared",
			},
		},
		th: {
			'app': "เริ่มต้น",
			'path': {
				'home': "หน้าแรก",
				'test-boxes': "ทดสอบกล่อง",
				'test-boxes-shared': "ทดสอบกล่องใช้ร่วมกัน",
			},
		},
	},
})

// app
const app = createApp(App)
app
	.use(i18n)
	.use(router)
	.component('Breadcrumbs', Breadcrumbs)
	.mount('#app')

