import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: '/',
			name: 'home',
			component: () => import('../pages/index.vue'),
		},
		{
			path: '/test-boxes',
			name: 'test-boxes',
			component: () => import('../pages/test-boxes.vue'),
		},
		{
			path: '/test-boxes-shared',
			name: 'test-boxes-shared',
			component: () => import('../pages/test-boxes-shared.vue'),
		},
	]
})

export default router
