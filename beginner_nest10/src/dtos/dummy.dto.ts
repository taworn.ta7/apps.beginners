import {
	IsNotEmpty,
	MinLength,
	MaxLength,
	IsEmail,
} from 'class-validator';

export class DummyDto {
	@IsNotEmpty()
	@MinLength(1)
	@MaxLength(200)
	description: string;
}

export class SendMailDto {
	@IsEmail()
	mail: string;
}
