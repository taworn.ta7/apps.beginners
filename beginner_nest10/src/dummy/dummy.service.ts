import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Dummy } from '../schemas/dummy.entity';
import { DummyDto } from '../dtos/dummy.dto';

@Injectable()
export class DummyService {

	private readonly logger = new Logger(DummyService.name);

	constructor(
		@InjectRepository(Dummy)
		private readonly dummyRepo: Repository<Dummy>,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Adds dummy data.
	 *
	 * @returns The dummy data added.
	 */
	async add(dto: DummyDto): Promise<Dummy> {
		// adds dummy row
		const dummy = new Dummy(dto);
		await this.dummyRepo.save(dummy);
		return dummy;
	}

	/**
	 * Lists dummies data.
	 *
	 * @returns The dummies data returned.
	 */
	async list(): Promise<Dummy[]> {
		// loads dummies row(s)
		return await this.dummyRepo.find({
			order: {
				created: 'DESC',
			},
		});
	}

}
