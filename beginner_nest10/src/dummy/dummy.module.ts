import { Module } from '@nestjs/common';
import { SchemasModule } from '../schemas/schemas.module';
import { DummyService } from './dummy.service';
import { DummyController } from './dummy.controller';

@Module({
	imports: [
		SchemasModule,
	],
	providers: [
		DummyService,
	],
	controllers: [
		DummyController,
	],
})
export class DummyModule { }
