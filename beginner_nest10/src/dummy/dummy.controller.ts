import {
	Logger,
	Controller,
	Req,
	Res,
	Body,
	Get,
	Post,
	UseInterceptors,
} from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { Request, Response } from 'express';
import { DumpHeaderInterceptor, DumpBodyInterceptor } from '../dump.interceptor';
import { Dummy } from '../schemas/dummy.entity';
import { DummyDto, SendMailDto } from '../dtos/dummy.dto';
import { DummyService } from './dummy.service';

@Controller(`/api/dummy`)
export class DummyController {

	private readonly logger = new Logger(DummyController.name);

	constructor(
		private readonly mailerService: MailerService,
		private readonly service: DummyService,
	) { }

	// ----------------------------------------------------------------------

	@Post('add')
	@UseInterceptors(DumpHeaderInterceptor)
	@UseInterceptors(DumpBodyInterceptor)
	async add(
		@Req() req: Request,
		@Body('dummy') dto: DummyDto,
	): Promise<{
		dummy: Dummy,
	}> {
		const dummy = await this.service.add(dto);
		this.logger.verbose(`${req.id}; dummy added: ${JSON.stringify(dummy, null, 2)}`);
		return {
			dummy,
		};
	}

	@Get()
	async list(
		@Req() req: Request,
	): Promise<{
		dummies: Dummy[],
	}> {
		const dummies = await this.service.list();
		return {
			dummies,
		};
	}

	// ----------------------------------------------------------------------

	@Post('send-mail')
	async sendMail(
		@Req() req: Request,
		@Body() dto: SendMailDto,
	): Promise<
		void
	> {
		// sends email
		await this.mailerService.sendMail({
			to: dto.mail,
			subject: `Test Mail`,
			template: 'test',
			context: {
				mail: dto.mail,
			},
		});
	}

}
