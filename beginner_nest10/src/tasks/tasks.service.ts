import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { Timeout, Interval } from '@nestjs/schedule';
import { DataSource, LessThan } from 'typeorm';
import * as path from 'node:path';
import * as fs from 'node:fs';
import { Dummy } from '../schemas/dummy.entity';

const delayTime = 1;
const intervalTime = 8 * 60 * 60 * 1000;
const env = process.env;

@Injectable()
export class TasksService {

	private readonly logger = new Logger(TasksService.name);

	private logDir: string;

	constructor(
		private readonly dataSource: DataSource,
	) {
		this.logDir = path.resolve(path.join(__dirname, '..', '..', env.LOG_DIR));
		this.logger.debug(`logs path: ${this.logDir}`);
		this.logger.debug(`days to keep logs: ${+env.DAYS_TO_KEEP_LOGS} day(s)`);
		this.logger.debug(`days to keep dummy data: ${+env.DAYS_TO_KEEP_DUMMY} day(s)`);
	}

	// ----------------------------------------------------------------------

	@Timeout(delayTime)
	async handleTimeout() {
		await this.deleteLogs(+env.DAYS_TO_KEEP_LOGS, this.logDir);
		await this.deleteDummy(+env.DAYS_TO_KEEP_DUMMY);
	}

	@Interval(intervalTime)
	async handleInterval() {
		await this.deleteLogs(+env.DAYS_TO_KEEP_LOGS, this.logDir);
		await this.deleteDummy(+env.DAYS_TO_KEEP_DUMMY);
	}

	// ----------------------------------------------------------------------

	daysToKeepToDate(daysToKeep: number): Date {
		const now = new Date();
		const nowNoTime = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		return new Date(nowNoTime.valueOf() - (daysToKeep * 24 * 60 * 60 * 1000));
	}

	dateFormat(d: Date): string {
		return d.getFullYear() + '-' + (d.getMonth() + 1).toString().padStart(2, '0') + '-' + d.getDate().toString().padStart(2, '0');
	}

	// ----------------------------------------------------------------------

	async deleteLogs(daysToKeep: number, folder: string) {
		// checks before execute, days to keep must more than zero
		if (daysToKeep <= 0)
			return;

		// computes date range
		const date = this.daysToKeepToDate(daysToKeep);

		// cleans obsolete data
		this.logger.debug(`logs older than ${this.dateFormat(date)} will be delete!`);
		const files = await fs.promises.readdir(folder);
		const re = /^([0-9]{4})([0-9]{2})([0-9]{2})\.log$/;
		for (let i = 0; i < files.length; i++) {
			const file = files[i];
			const found = file.match(re);
			if (found) {
				const d = new Date(Number(found[1]), Number(found[2]) - 1, Number(found[3]));
				if (d.valueOf() < date.valueOf()) {
					this.logger.debug(`delete: ${file}`);
					await fs.promises.rm(path.join(folder, file));
				}
			}
		}
	};

	async deleteDummy(daysToKeep: number) {
		// checks before execute, days to keep must more than zero
		if (daysToKeep <= 0)
			return;

		// computes date range
		const date = this.daysToKeepToDate(daysToKeep);

		// cleans obsolete data
		this.logger.debug(`dummy data older than ${this.dateFormat(date)} will be delete!`);
		await this.dataSource.getRepository(Dummy).delete({ updated: LessThan(date) })
	};

}
