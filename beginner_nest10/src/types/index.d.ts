import { Express, Request } from 'express';

declare global {
	namespace Express {
		export interface Request {
			// adds request id field
			id?: string;

			// NOTE:
			// If you have authentication,
			// adds 'member' field.
		}
	}
}
