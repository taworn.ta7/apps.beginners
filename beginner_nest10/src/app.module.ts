import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ScheduleModule } from '@nestjs/schedule';
import { MulterModule } from '@nestjs/platform-express';
import { MailerModule } from '@nestjs-modules/mailer';
import { EjsAdapter } from '@nestjs-modules/mailer/dist/adapters/ejs.adapter';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { join } from 'path';
import { TasksModule } from './tasks/tasks.module';
import { DummyModule } from './dummy/dummy.module';
import { AppService } from './app.service';
import { AppController } from './app.controller';

const env = process.env;

@Module({
	imports: [
		// config module
		ConfigModule.forRoot({
			envFilePath: [
				'.env.override',
				'.env',
			],
		}),

		// serve static module
		ServeStaticModule.forRoot({
			rootPath: join(__dirname, '..', env.UPLOAD_DIR),
			serveRoot: `/${process.env.UPLOAD_DIR}`,
		}),

		// schedule task module
		ScheduleModule.forRoot(),

		// multer module
		MulterModule.registerAsync({
			useFactory: () => ({
				dest: join(__dirname, '..', env.UPLOAD_DIR),
			}),
		}),

		// mailer module
		MailerModule.forRootAsync({
			useFactory: () => ({
				transport: {
					host: env.MAIL_HOST,
					port: +env.MAIL_PORT,
					auth: {
						user: env.MAIL_USER,
						pass: env.MAIL_PASS,
					}
				},
				defaults: {
					from: env.MAIL_ADMIN,
				},
				template: {
					dir: `${__dirname}/mail_templates`,
					adapter: new EjsAdapter(),
					options: {
						strict: false,
					},
				},
			}),
		}),

		// database typeorm module
		TypeOrmModule.forRootAsync({
			useFactory: () => {
				const type = <'sqlite' | 'mysql'>env.DB_USE;
				let db: string
				if (type === 'sqlite')
					db = join(__dirname, '..', env.STORAGE_DIR, env.DB_FILE);
				else
					db = env.DB_NAME;

				const options: TypeOrmModuleOptions = {
					type,
					host: env.DB_HOST,
					port: +env.DB_PORT,
					username: env.DB_USER,
					password: env.DB_PASS,
					database: db,
					entities: [
						'dist/**/*.entity.{ts,js}',
					],
					synchronize: true,
					retryAttempts: 0,
					//logging: true,
				};
				return options;
			},
		}),

		// tasks schedule
		TasksModule,

		// dummy module
		DummyModule,
	],
	providers: [
		AppService,
	],
	controllers: [
		AppController,
	],
})
export class AppModule { }
