import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dummy } from './dummy.entity';

@Module({
	imports: [
		TypeOrmModule.forFeature([
			Dummy,
			// TODO:
			// more database table(s)
		]),
	],
	exports: [
		TypeOrmModule,
	],
})
export class SchemasModule { }
