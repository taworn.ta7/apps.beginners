import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
} from 'typeorm';

/**
 * Dummy data record.
 */
@Entity({
	name: 'dummy',
})
export class Dummy {

	@PrimaryGeneratedColumn({
		type: 'int',
	})
	id: number;

	// ----------------------------------------------------------------------

	/**
	 * Just type any text.
	 */
	@Column({
		length: 200,
	})
	description: string;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<Dummy>) {
		Object.assign(this, o)
	}

}
