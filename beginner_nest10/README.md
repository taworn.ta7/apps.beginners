# beginner_nest10

Beginner code for Nest 10.


## Features

* Logging with colorize
* Config files, .env and .env.override to edit
* HTTP exception with more information
* Mail template and send mail sample
* Background task scheduled to delete unused data table(s)


## Run Server

To load packages, just run this once:

	npm i

Please wait here for a long long time. Y_Y

To run:

	npm run start

Or, use this command to reload automatically

	npm run start:debug

Or, build and run release

	npm run build
	npm run start:prod

You can use [Postman](https://www.postman.com), load script files (beginner.postman_*) and test.


## Generate Document

Run this command:

	npm run doc

After document generated, open browser to:

	localhost:8800

Not much document, tough.


## Configuration

Editing in '.env.override', if you want.

* LOG_TO_CONSOLE: log out to console, 0 or 1
* LOG_TO_FILE: log out to file, 0 or 1
* DB_USE: choose database, 'sqlite' or 'mysql'
* DB_HOST: database host
* DB_PORT: database port
* DB_USER: database user
* DB_PASS: database password
* MAIL_HOST: mail sender host
* MAIL_PORT: mail sender port
* MAIL_USER: mail sender user
* MAIL_PASS: mail sender password
* HTTP_PORT: HTTP port, may change this to 80
* DAYS_TO_KEEP_LOGS: number of days to keep old log file
* DAYS_TO_KEEP_DUMMY: number of days to keep old testing 'dummy' table

Note #1: If you use MySQL, don't forget to create blank database 'beginner'.

Note #2: I used service [ethereal](https://ethereal.email), which is a fake mail service.
And, some time, your user and password will expire and you have to recreate them.  It's free.
After created ethereal account, copy user and password to '.env.override'.


## Last

Sorry, but I'm not good at English. T_T

