# beginner_go1

Beginner code for Go 1.


## Features

* Logging with colorize
* Config files, config.yaml and config.override.yaml to edit
* HTTP exception with more information
* Mail template and send mail sample
* Background task scheduled to delete unused data table(s)


## Run

Type:

	go run .

You can use [Postman](https://www.postman.com), load script files (beginner.postman_*) and test.


## Generate Document

If you want to generate document on server.  Install godoc:

	go install -v golang.org/x/tools/cmd/godoc@latest

Then, type:

	godoc -http=:8800

And opens browser to URL:

	localhost:8800

Port number can be change.


## Configuration

Editing in 'config.override.yaml', if you want.

* db -> host: database host
* db -> port: database port
* db -> user: database user
* db -> pass: database password
* mail -> host: mail sender host
* mail -> port: mail sender port
* mail -> user: mail sender user
* mail -> pass: mail sender password
* clean -> daysToKeepDummies: number of days to keep old testing 'dummy' table

Note #1: If you use MySQL, don't forget to create blank database 'beginner'.

Note #2: I used service [ethereal](https://ethereal.email), which is a fake mail service.
And, some time, your user and password will expire and you have to recreate them.  It's free.
After created ethereal account, copy user and password to 'config.override.yaml'.


## Last

Sorry, but I'm not good at English. T_T

