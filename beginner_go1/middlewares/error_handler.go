package middlewares

import (
	"errors"
	"net/http"
	"time"

	log "github.com/cihub/seelog"
	"github.com/gin-gonic/gin"

	"beginner/dbg"
	"beginner/http_errors"
)

// Error handling middleware.
func ErrorHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		if len(c.Errors) > 0 {
			id, _ := c.Get("id")

			e := c.Errors[0]

			{
				var err *http_errors.LocaleError
				if errors.As(e, &err) {
					json := gin.H{
						"statusCode": err.StatusCode,
						"locales": gin.H{
							"en": err.En,
							"th": err.Th,
						},
						"path":      c.Request.URL.Path,
						"requestId": id,
						"timestamp": time.Now(),
					}
					c.AbortWithStatusJSON(err.StatusCode, json)
					log.Warnf("%v; %v %v: %v %v; [ERROR] %v",
						id, c.Request.Method, c.Request.RequestURI,
						err.StatusCode, http.StatusText(err.StatusCode), err.En)
					return
				}
			}

			{
				var err *http_errors.StatusError
				if errors.As(e, &err) {
					json := gin.H{
						"statusCode":  err.StatusCode,
						"description": err.Description,
						"path":        c.Request.URL.Path,
						"requestId":   id,
						"timestamp":   time.Now(),
					}
					c.AbortWithStatusJSON(err.StatusCode, json)
					log.Warnf("%v; %v %v: %v %v; [ERROR] %v",
						id, c.Request.Method, c.Request.RequestURI,
						err.StatusCode, http.StatusText(err.StatusCode), err.Description)
					return
				}
			}

			json := gin.H{
				"statusCode": http.StatusInternalServerError,
				"path":       c.Request.URL.Path,
				"requestId":  id,
				"timestamp":  time.Now(),
			}
			c.AbortWithStatusJSON(http.StatusInternalServerError, json)
			log.Warnf("%v; %v %v: %v %v; [ERROR] %v",
				id, c.Request.Method, c.Request.RequestURI,
				http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), dbg.Dump(e))
		}
	}
}
