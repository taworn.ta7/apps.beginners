package middlewares

import (
	log "github.com/cihub/seelog"
	"github.com/gin-gonic/gin"

	"beginner/helpers"
)

// Logging middleware.
func Logging() gin.HandlerFunc {
	return func(c *gin.Context) {
		id := helpers.GenerateUuid()
		c.Set("id", id)
		log.Infof("%v; %v %v...", id, c.Request.Method, c.Request.RequestURI)

		c.Next()

		if !c.IsAborted() {
			log.Infof("%v; %v %v: DONE", id, c.Request.Method, c.Request.RequestURI)
		}
	}
}
