package middlewares

import (
	log "github.com/cihub/seelog"
	"github.com/gin-gonic/gin"

	"beginner/dbg"
)

// Dumps HTTP headers and body middleware.
func Dump(dumpHeader bool, dumpBody bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		id, _ := c.Get("id")

		if dumpHeader {
			log.Debugf("%v; Header: %v", id, dbg.Dump(c.Request.Header))
		}

		if dumpBody {
			log.Debugf("%v; Body: %#v", id, c.Request.Body)
		}

		c.Next()
	}
}
