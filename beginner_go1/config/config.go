// Package configuration.
package config

import (
	"fmt"
	"strings"

	log "github.com/cihub/seelog"
	"github.com/cristalhq/aconfig"
	"github.com/cristalhq/aconfig/aconfigyaml"
)

// Configuration structure data type.
type ConfigType struct {
	// app
	App struct {
		Name       string `yaml:"name"`
		Version    string `yaml:"version"`
		StorageDir string `yaml:"storageDir"`
		UploadDir  string `yaml:"uploadDir"`
	}

	// database config
	Db struct {
		Use  string `yaml:"use"` // currently: sqlite or mysql, but due to bug in sqlite, only mysql for now
		Host string `yaml:"host"`
		Port int    `yaml:"port"`
		User string `yaml:"user"`
		Pass string `yaml:"pass"`
		Name string `yaml:"name"` // database name, in case of mysql
		File string `yaml:"file"` // database file, in case of sqlite
	}

	// mail config
	Mail struct {
		Host  string `yaml:"host"`
		Port  int    `yaml:"port"`
		User  string `yaml:"user"`
		Pass  string `yaml:"pass"`
		Admin string `yaml:"admin"` // administrator
	}

	// HTTP config
	Http struct {
		Port int `yaml:"port"`
	}

	// cleaning config
	Clean struct {
		DaysToKeepDummies int `yaml:"daysToKeepDummies"` // number of days to keep old records in member_signup table
	}
}

var config *ConfigType

// ----------------------------------------------------------------------

// Retrieves the configuration.
func Get() *ConfigType {
	return config
}

// Logging out the configuration.
func Log() {
	log.Info("Configuration:")
	log.Info("  App:")
	log.Info(fmt.Sprintf("    Name: %v", config.App.Name))
	log.Info(fmt.Sprintf("    Version: %v", config.App.Version))
	log.Info(fmt.Sprintf("    StorageDir: %v", config.App.StorageDir))
	log.Info(fmt.Sprintf("    UploadDir: %v", config.App.UploadDir))
	log.Info("  Db:")
	log.Info(fmt.Sprintf("    Use: %v", config.Db.Use))
	log.Info(fmt.Sprintf("    Host: %v", config.Db.Host))
	log.Info(fmt.Sprintf("    Port: %v", config.Db.Port))
	log.Info(fmt.Sprintf("    User: %v", config.Db.User))
	log.Info(fmt.Sprintf("    Pass: %v", "**** << HIDDEN >> ****"))
	log.Info(fmt.Sprintf("    Name: %v", config.Db.Name))
	log.Info(fmt.Sprintf("    File: %v", config.Db.File))
	log.Info("  Mail:")
	log.Info(fmt.Sprintf("    Host: %v", config.Mail.Host))
	log.Info(fmt.Sprintf("    Port: %v", config.Mail.Port))
	log.Info(fmt.Sprintf("    User: %v", config.Mail.User))
	log.Info(fmt.Sprintf("    Pass: %v", "**** << HIDDEN >> ****"))
	log.Info(fmt.Sprintf("    Admin: %v", config.Mail.Admin))
	log.Info("  Http:")
	log.Info(fmt.Sprintf("    Port: %v", config.Http.Port))
	log.Info("  Clean:")
	log.Info(fmt.Sprintf("    DaysToKeepDummies: %v", config.Clean.DaysToKeepDummies))
	log.Flush()
}

// Printouts the configuration.
func Print() string {
	b := strings.Builder{}
	b.WriteString("Configuration:\n")
	b.WriteString("  App:\n")
	b.WriteString(fmt.Sprintf("    Name: %v\n", config.App.Name))
	b.WriteString(fmt.Sprintf("    Version: %v\n", config.App.Version))
	b.WriteString(fmt.Sprintf("    StorageDir: %v\n", config.App.StorageDir))
	b.WriteString(fmt.Sprintf("    UploadDir: %v\n", config.App.UploadDir))
	b.WriteString("  Db:\n")
	b.WriteString(fmt.Sprintf("    Use: %v\n", config.Db.Use))
	b.WriteString(fmt.Sprintf("    Host: %v\n", config.Db.Host))
	b.WriteString(fmt.Sprintf("    Port: %v\n", config.Db.Port))
	b.WriteString(fmt.Sprintf("    User: %v\n", config.Db.User))
	b.WriteString(fmt.Sprintf("    Pass: %v\n", "**** << HIDDEN >> ****"))
	b.WriteString(fmt.Sprintf("    Name: %v\n", config.Db.Name))
	b.WriteString(fmt.Sprintf("    File: %v\n", config.Db.File))
	b.WriteString("  Mail:\n")
	b.WriteString(fmt.Sprintf("    Host: %v\n", config.Mail.Host))
	b.WriteString(fmt.Sprintf("    Port: %v\n", config.Mail.Port))
	b.WriteString(fmt.Sprintf("    User: %v\n", config.Mail.User))
	b.WriteString(fmt.Sprintf("    Pass: %v\n", "**** << HIDDEN >> ****"))
	b.WriteString(fmt.Sprintf("    Admin: %v\n", config.Mail.Admin))
	b.WriteString("  Http:\n")
	b.WriteString(fmt.Sprintf("    Port: %v\n", config.Http.Port))
	b.WriteString("  Clean:\n")
	b.WriteString(fmt.Sprintf("    DaysToKeepDummies: %v\n", config.Clean.DaysToKeepDummies))
	return b.String()
}

// ----------------------------------------------------------------------

// Setup the configuration.
func Setup() *ConfigType {
	var c ConfigType
	loader := aconfig.LoaderFor(&c, aconfig.Config{
		Files: []string{
			"config.yaml",
			"config.override.yaml",
		},
		FileDecoders: map[string]aconfig.FileDecoder{
			// from `aconfigyaml` submodule
			// see submodules in repo for more formats
			".yaml": aconfigyaml.New(),
		},
		AllowUnknownFields: true,
		MergeFiles:         true,
	})
	if err := loader.Load(); err != nil {
		fmt.Println("config file(s) is not loaded!")
		panic(err)
	}

	config = &c
	return config
}
