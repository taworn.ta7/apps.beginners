package send_mail

import (
	"bytes"
	"html/template"
)

// Dummy mail template.
type DummyTemplate struct {
	Email string
}

// Uses dummy mail template.
func Dummy(value *DummyTemplate) string {
	if dummy == nil {
		dummy = createTemplateDummy()
	}
	t := dummy

	var buffer bytes.Buffer
	t.Execute(&buffer, value)
	body := buffer.String()

	return body
}

// ----------------------------------------------------------------------

var dummy *template.Template = nil

func createTemplateDummy() *template.Template {
	t, err := template.New("dummy").Parse(`
<html>

<head>
	<title>Test Send Mail</title>
</head>

<body>
	<p>You have received test mail.</p>
	<p>Your email is {{.Email}}, right?</p>
</body>

</html>
`)
	if err != nil {
		panic(err)
	}
	return t
}
