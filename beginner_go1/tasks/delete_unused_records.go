package tasks

import (
	"time"

	log "github.com/cihub/seelog"

	"beginner/config"
	"beginner/dbi"
	"beginner/schemas"
)

func deleteUnusedRecords() {
	conf := config.Get()
	deleteUnusedDummies(conf.Clean.DaysToKeepDummies)
}

// ----------------------------------------------------------------------

func deleteUnusedDummies(daysToKeep int) {
	db := dbi.Get()

	// checks before execute, days to keep must keep at least 0 (today)
	if daysToKeep <= 0 {
		return
	}

	// computes date range
	now := time.Now()
	end := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local)
	begin := end.AddDate(0, 0, -daysToKeep)

	// cleans old dummy table
	log.Debugf("dummy records older than %v will be delete!", begin)
	db.Unscoped().Where("updated_at < ?", begin).Delete(&schemas.Dummy{})
}
