package main

import (
	"fmt"
	"os"
	"runtime"

	log "github.com/cihub/seelog"
	"github.com/fatih/color"
	"github.com/gin-gonic/gin"
	"github.com/mattn/go-colorable"

	"beginner/config"
	"beginner/dbg"
	"beginner/dbi"
	"beginner/middlewares"
	"beginner/routes"
	"beginner/routes/dummy"
	"beginner/tasks"
)

func init() {
	// initialize logging
	l, e := log.LoggerFromConfigAsFile("seelog.xml")
	if e != nil {
		panic(e)
	}
	log.ReplaceLogger(l)
}

func main() {
	defer log.Flush()
	defer log.Info("graceful shutdown ^_^")

	// setup subsystems
	dbg.Setup()
	conf := config.Setup()
	dbi.Setup()

	// greeting
	color.HiWhite("Beginner, Hello ^_^")
	fmt.Println()
	color.HiCyan(config.Print())
	config.Log()
	fmt.Println()

	log.Trace("Trace...")
	log.Debug("Debug...")
	log.Info("Info...")
	log.Warn("Warn...")
	log.Error("Error...")
	log.Critical("Critical...")

	// gin setup
	gin.ForceConsoleColor()
	//gin.DisableConsoleColor()
	if runtime.GOOS == "windows" {
		gin.DefaultWriter = colorable.NewColorableStdout()
	} else {
		gin.DefaultWriter = os.Stdout
	}

	// router setup
	router := gin.Default()
	router.Use(middlewares.CORSMiddleware())
	router.Use(middlewares.Logging())
	router.Use(middlewares.ErrorHandler())

	// set a lower memory limit for multipart forms (default is 32 MiB)
	router.MaxMultipartMemory = 10 << 20 // 10 MiB

	// web routes
	{
		web := router.Group("/api")
		web.GET("/about", routes.About, middlewares.Dump(true, true))
		web.GET("/config", routes.Config)
	}
	{
		web := router.Group("/api/dummy")
		web.POST("/add", dummy.Add)
		web.GET("/", dummy.List)
		web.POST("/send-mail", dummy.SendMail)
	}

	// prepares timer
	go tasks.BackgroundTasks()

	// run server
	router.Run(fmt.Sprintf(":%v", conf.Http.Port))
}
