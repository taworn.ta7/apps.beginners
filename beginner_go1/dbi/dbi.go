// Database interface package.
package dbi

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"beginner/config"
	"beginner/schemas"
)

var db *gorm.DB

// Retrieves the database interface.
func Get() *gorm.DB {
	return db
}

// ----------------------------------------------------------------------

// Setup the database interface.
func Setup() *gorm.DB {
	c := config.Get()

	// logging
	l := logger.New(
		log.New(os.Stdout, "\n", log.LstdFlags),
		logger.Config{
			SlowThreshold:             time.Second,   // slow SQL threshold
			LogLevel:                  logger.Silent, // log level
			IgnoreRecordNotFoundError: true,          // ignore ErrRecordNotFound error for logger
			Colorful:                  true,          // disable color
		},
	)

	var err error
	if c.Db.Use == "mysql" {
		// mysql connection
		dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local",
			c.Db.User, c.Db.Pass, c.Db.Host, c.Db.Port, c.Db.Name)
		db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
			Logger: l,
		})
	} else {
		// sqlite connection
		dsn := fmt.Sprintf("%v", filepath.Join(c.App.StorageDir, c.Db.File))
		db, err = gorm.Open(sqlite.Open(dsn), &gorm.Config{
			Logger: l,
		})
	}
	if err != nil {
		fmt.Println("GORM is not initialize!")
		panic(err)
	}

	db.AutoMigrate(&schemas.Dummy{})

	return db
}
