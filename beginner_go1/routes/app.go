package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"beginner/config"
)

// Returns server name and version.
func About(c *gin.Context) {
	conf := config.Get()
	c.JSON(http.StatusOK, gin.H{
		"app":     conf.App.Name,
		"version": conf.App.Version,
	})
}

// Returns current configuration.
func Config(c *gin.Context) {
	conf := config.Get()
	c.JSON(http.StatusOK, gin.H{
		"config": conf,
	})
}
