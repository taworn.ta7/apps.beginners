package dummy

import (
	"net/http"

	log "github.com/cihub/seelog"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"beginner/config"
	"beginner/dbg"
	"beginner/dbi"
	"beginner/helpers"
	"beginner/http_errors"
	"beginner/schemas"
	"beginner/send_mail"
)

// Dummy DTO type.
type DummyDtoType struct {
	Dummy struct {
		Description string `json:"description" binding:"required" validate:"required,min=1,max=200"`
	} `json:"dummy" binding:"required"`
}

// Send mail DTO type.
type SendMailDtoType struct {
	Mail string `json:"mail" binding:"required" validate:"required"`
}

// ----------------------------------------------------------------------

// Adds dummy data.
func Add(c *gin.Context) {
	//conf := config.Get()
	db := dbi.Get()
	id, _ := c.Get("id")

	// binds body to JSON
	var json DummyDtoType
	if err := c.ShouldBindJSON(&json); err != nil {
		c.Error(http_errors.Err400JSONSyntax)
		return
	}
	if !helpers.Validate(c, json) {
		return
	}
	log.Debugf("%v; JSON: %v", id, dbg.Dump(json))

	// create
	dummy := schemas.Dummy{
		Model:       gorm.Model{},
		Description: json.Dummy.Description,
	}

	// saves
	db.Create(&dummy)

	c.JSON(http.StatusOK, gin.H{
		"dummy": dummy,
	})
}

// Lists dummies data.
func List(c *gin.Context) {
	//conf := config.Get()
	db := dbi.Get()
	//id, _ := c.Get("id")

	var dummies []schemas.Dummy
	db.
		Order("ID DESC").
		Find(&dummies)

	c.JSON(http.StatusOK, gin.H{
		"dummies": dummies,
	})
}

// ----------------------------------------------------------------------

// Tests send mail.
func SendMail(c *gin.Context) {
	conf := config.Get()
	//db := dbi.Get()
	id, _ := c.Get("id")

	// binds body to JSON
	var json SendMailDtoType
	if err := c.ShouldBindJSON(&json); err != nil {
		c.Error(http_errors.Err400JSONSyntax)
		return
	}
	if !helpers.Validate(c, json) {
		return
	}
	log.Debugf("%v; JSON: %v", id, dbg.Dump(json))

	send_mail.SendMail(&send_mail.MailSend{
		From:    conf.Mail.Admin,
		To:      json.Mail,
		Subject: "Test Mail",
		Body: send_mail.Dummy(&send_mail.DummyTemplate{
			Email: json.Mail,
		}),
	})

	c.JSON(http.StatusOK, gin.H{})
}
