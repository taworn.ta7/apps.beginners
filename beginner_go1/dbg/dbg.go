// Package debugging.
package dbg

import (
	"github.com/davecgh/go-spew/spew"
)

var inspector spew.ConfigState

// ----------------------------------------------------------------------

// Dumps a variable.
func Dump(a ...interface{}) string {
	return inspector.Sdump(a)
}

// ----------------------------------------------------------------------

// Setup the debugging.
func Setup() {
	inspector = spew.ConfigState{
		Indent: "  ",
	}
}
