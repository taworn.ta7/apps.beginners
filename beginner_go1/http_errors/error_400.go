package http_errors

import (
	"net/http"
)

var (
	//
	// 400 Bad Request
	//

	Err400JSONSyntax = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `JSON is malform!`,
		Th:         `JSON ไม่ถูกต้อง!`,
	}

	Err400ValidationFailed = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Validation is failed!`,
		Th:         `การตรวจสอบข้อมูลไม่ถูกต้อง!`,
	}

	Err400UploadIsNotFound = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Uploaded file is not found!`,
		Th:         `ไม่มีไฟล์ที่ Upload ขึ้นมา!`,
	}

	Err400UploadIsNotType = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Uploaded file is not a specify type!`,
		Th:         `ไฟล์ที่ Upload ขึ้นมา ไม่ใช่ไฟล์ชนิดที่ต้องการ!`,
	}

	Err400UploadIsNotTypeImage = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Uploaded file is not an image file!`,
		Th:         `ไฟล์ที่ Upload ขึ้นมา ไม่ใช่ไฟล์รูปภาพ!`,
	}

	Err400UploadIsTooBig = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Uploaded file size is too big!`,
		Th:         `ไฟล์ที่ Upload ขึ้นมา มีขนาดใหญ่เกินไป!`,
	}
)
