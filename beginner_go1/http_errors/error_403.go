package http_errors

import (
	"net/http"
)

var (
	//
	// 403 Forbidden
	//

	Err403AlreadyExists = &LocaleError{
		StatusCode: http.StatusForbidden,
		En:         `Data is already exists!`,
		Th:         `มีข้อมูลที่ต้องการอยู่แล้ว!`,
	}
)
