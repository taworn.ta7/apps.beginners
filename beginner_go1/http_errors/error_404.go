package http_errors

import (
	"net/http"
)

var (
	//
	// 404 Not Found
	//

	Err404NotFound = &LocaleError{
		StatusCode: http.StatusNotFound,
		En:         `Data is not found!`,
		Th:         `ไม่พบข้อมูลที่ต้องการ!`,
	}
)
