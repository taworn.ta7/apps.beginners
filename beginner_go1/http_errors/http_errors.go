// Package HTTP errors.
package http_errors

import (
	"fmt"
)

// Simple status error type.
type StatusError struct {
	StatusCode  int
	Description string
}

func (e *StatusError) Error() string {
	if len(e.Description) > 0 {
		return e.Description
	} else {
		return fmt.Sprintf("HTTP status code: %v", e.StatusCode)
	}
}

// ----------------------------------------------------------------------

// Locale-based error type.
type LocaleError struct {
	StatusCode int
	En         string
	Th         string
}

func (e *LocaleError) Error() string {
	return e.En
}
