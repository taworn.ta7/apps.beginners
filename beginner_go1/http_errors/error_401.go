package http_errors

import (
	"net/http"
)

var (
	//
	// 401 Unauthorized
	//

	Err401Timeout = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Timeout, you need to sign-in again!`,
		Th:         `หมดเวลา, คุณต้อง sign-in เข้าระบบอีกครั้ง!`,
	}

	Err401NeedSignIn = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `You require sign-in!`,
		Th:         `คุณต้อง sign-in เข้าระบบ!`,
	}

	Err401EmailOrPasswordInvalid = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Your email or password is incorrect!`,
		Th:         `คุณใส่อีเมลหรือรหัสผ่านไม่ถูกต้อง!`,
	}

	Err401MemberIsResigned = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Your membership is resigned!`,
		Th:         `คุณลาออกจากระบบไปแล้ว!`,
	}

	Err401MemberIsDisabledByAdmin = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Your membership is disabled!`,
		Th:         `คุณถูกระงับการใช้งาน!`,
	}

	Err401PasswordIsIncorrect = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Password is incorrect!`,
		Th:         `รหัสผ่านไม่ถูกต้อง!`,
	}

	Err401MemberRequired = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Member rights is required!`,
		Th:         `ต้องการสิทธิ Member!`,
	}

	Err401AdminRequired = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Admin rights is required!`,
		Th:         `ต้องการสิทธิ Admin!`,
	}

	Err401SignInExternal = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Sign-in external error!`,
		Th:         `การล๊อกอินข้างนอกล้มเหลว!`,
	}
)
