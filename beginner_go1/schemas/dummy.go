// Package for database schemas.
package schemas

import (
	"time"

	"gorm.io/gorm"
)

// Dummy record.
type Dummy struct {
	gorm.Model  `json:"-"`
	ID          uint      `json:"id" binding:"required"`
	Description string    `json:"description" binding:"required" gorm:"type:varchar(200)"`
	CreatedAt   time.Time `json:"created" gorm:"autoCreateTime"`
	UpdatedAt   time.Time `json:"updated" gorm:"autoUpdateTime"`
}
